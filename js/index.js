$(function(){
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover();
	//El interval es el tiempo en milisegundos
	$('.carousel').carousel({
		interval:2000
	});
	//el ON se usa para usar eventos en JQuery--la letra e es
	//para que la funcion de JQuery reciba parametros
	$('#btnreserva').on('show.bs.modal', function(e){
		console.log('el modal contacto se esta mostrando');
	$('#btnreserva').removeClass('btn-primary');
	$('#btnreserva').addClass('btn-outline-success');
	$('#btnreserva').prop('disabled',true);
	});
	$('#btnreserva').on('shown.bs.modal', function(e){
		console.log('el modal contacto se mostró');
	});
	$('#btnreserva').on('hide.bs.modal', function(e){
		console.log('el modal contacto se oculta');
	});
	$('#btnreserva').on('hidden.bs.modal', function(e){
		console.log('el modal contacto se oculto');
		$('#btnreserva').removeClass('btn-outline-success');
		$('#btnreserva').addClass('btn-primary');
		$('#btnreserva').prop('disabled',false);
	});
});
